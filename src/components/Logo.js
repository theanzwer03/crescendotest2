import React from 'react';
import Logo1 from '../images/logo1.png';
import Logo2 from '../images/logo2.png';

export default function Logo() {
  return (
    <div className="logo">
      <div>
        <img alt=""  src={Logo1} />
        <img alt="" src={Logo2} />
      </div>
    </div>
  )
}