import React from 'react';
import sprite from '../images/sprite.svg';
/*
  props: icon,
*/
const SVG = ({icon, onClick}) => {
  return (
    <svg onClick={()=> onClick ? onClick() : null}>
      <use xlinkHref={`${sprite}#${icon}`}></use>
    </svg>
  )
}

export default SVG;