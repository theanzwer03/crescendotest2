import React from 'react';
import {Link} from "react-router-dom";
import { Carousel } from 'react-responsive-carousel';
import Card1 from '../images/card1.png';
import Card2 from '../images/card2.png';
import Card3 from '../images/card3.png';

export default function Content(){
  return (
    <>
      <section className="section1">
        <div>
          <h2>ACME Wealth <br/><span>Management Platforms</span></h2>
          <h4>Investment excellence.<br/>
          Diversity of thought.<br/>
          Organizational strength.
        </h4>  
        </div>
      </section>
      <section className="section2">
        <div>
          <h4>ACME Insights</h4>
          <p>How are factors being used around the world?</p>
          <Carousel className="carousel" showStatus={false} showThumbs={false} centerMode centerSlidePercentage={75}>
            <div className="card blue">
                <div className="img-holder">
                  <img alt="" src={Card1} />
                </div>
                <h4>Global Factor<br/>
                Investing Study</h4>
              </div>
              <div className="card green">
                <div className="img-holder">
                  <img alt="" src={Card2} />
                </div>
                <h4>2019<br/>
                Outlook</h4>
              </div>
              <div className="card cyan">
                <div className="img-holder">
                  <img alt="" src={Card3} />
                </div>
                <h4>Capital Market<br/>
                Assumptions</h4>
              </div>
          </Carousel>
          <div className="card-container">
            <div className="card blue">
              <div className="img-holder">
                <img alt="" src={Card1} />
              </div>
              <h4>Global Factor<br/>
              Investing Study</h4>
            </div>
            <div className="card green">
              <div className="img-holder">
                <img alt="" src={Card2} />
              </div>
              <h4>2019<br/>
              Outlook</h4>
            </div>
            <div className="card cyan">
              <div className="img-holder">
                <img alt="" src={Card3} />
              </div>
              <h4>Capital Market<br/>
              Assumptions</h4>
            </div>
          </div>
        </div>
      </section>
      <section className="section3">
        <div>
          <h2>Our Commitment to Professionals</h2>
          <h4>We help our partners deliver industry leading results with a commitment to excellence, thought-provoking insights and experienced distribution. We are laser focused on our shared goal – helping clients achieve their objectives. </h4>
          <button className="btn">Contact Us
          </button>
        </div>
      </section>
      <section className="section4">
        <div>
          <h2>Upcoming Events</h2>
          <p>This needs a great tagline, but I’ll fill it in later</p>
          <Carousel className="carousel" showStatus={false} showThumbs={false} centerMode centerSlidePercentage={75}>
          <div className="event-card">
              <ul className="date">
                <li>JAN</li>
                <li>28</li>
              </ul>
              <div className="content">
                <h4>Insight Exchange Network</h4>
                <p>Join us for this conference showcasing innovation.</p>
                <button className="btn">Get More Insight</button>
              </div>
              <div className="location"><b>Chicago</b>, IL</div>
            </div>

            <div className="event-card">
              <ul className="date">
                <li>FEB</li>
                <li>12</li>
              </ul>
              <div className="content">
                <h4>Citywide Buyer’s Retreat</h4>
                <p>Find out how banks are responding to the changing future of interest...</p>
                <button className="btn">Get More Insight
                </button>
              </div>
              <div className="location"><b>The Wagner</b>, New York </div>
            </div>
            <div className="event-card">
              <ul className="date">
                <li>MAY</li>
                <li>6</li>
              </ul>
              <div className="content">
                <h4>Research Exchange</h4>
                <p>Find the best online resources to help with your investments...</p>
                <button className="btn">Get More Insight
                </button>
              </div>
              <div className="location"><b>London</b>, England</div>
            </div>
          </Carousel>

          <div className="event-card-container">
            <div className="event-card">
              <ul className="date">
                <li>JAN</li>
                <li>28</li>
              </ul>
              <div className="content">
                <h4>Insight Exchange Network</h4>
                <p>Join us for this conference showcasing innovation.</p>
                <button className="btn">Get More Insight</button>
              </div>
              <div className="location">Chicago, IL</div>
            </div>

            <div className="event-card">
              <ul className="date">
                <li>FEB</li>
                <li>12</li>
              </ul>
              <div className="content">
                <h4>Citywide Buyer’s Retreat</h4>
                <p>Find out how banks are responding to the changing future of interest...</p>
                <button className="btn">Get More Insight
                </button>
              </div>
              <div className="location">The Wagner, New York </div>
            </div>
            <div className="event-card">
              <ul className="date">
                <li>MAY</li>
                <li>6</li>
              </ul>
              <div className="content">
                <h4>Research Exchange</h4>
                <p>Find the best online resources to help with your investments...</p>
                <button className="btn">Get More Insight
                </button>
              </div>
              <div className="location">London, England</div>
            </div>

          </div>
        </div>
      </section>
      <section className="footer-nav">
        <div>
          <nav className="active">
            <ul className="inline">
              <li>
                <Link to="">Privacy Policy</Link>
              </li>
              <li>
                <Link to="">Terms of Use</Link>
              </li>
            </ul>
            <Link to="" className="site-link">Site Link</Link>
            <ul>
              <li><Link to="">Home</Link></li>
              <li><Link to="">About Us</Link></li>
              <li><Link to="">Insights</Link></li>
              <li><Link to="">Events</Link></li>
              <li><Link to="">Contact Us</Link></li>
            </ul>
          </nav>
        </div>
      </section>
    </>
  )
}