import React from 'react';
import {BrowserRouter as Router}  from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import Header from './Header';
import Content from './Content';
import SVG from './SVG';

export default function App() {
  return (
    <Router>
      <div>
        <Header></Header>
        <Content></Content>
        <footer>
          <div>
            <div>
              <p>Call us at 111-222-3333<br/>
              for more information </p>
            </div>
            <div className="social-medias">
              <ul>
                <li>Social Share</li>
                <li><SVG icon="icon-facebook"/></li>
                <li><SVG icon="icon-twitter"/></li>
                <li><SVG icon="icon-linkedin"/></li>
                <li><SVG icon="icon-envelope"/></li>
              </ul>
            </div>
          </div>
        </footer>
      </div>
    </Router>
  )
}