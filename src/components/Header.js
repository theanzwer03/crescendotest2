import React, {useState} from 'react';
import Logo from './Logo';
import {Link}  from 'react-router-dom';

export default function Header() {
  const [isActive, setIsActive] = useState(false);
  const buttonOnClick = () => {
    if(!isActive){
      setIsActive(true);
    }
    else{
      setIsActive(false);
    }
  }
  return (
    <header>
      <Logo />
      <div onClick={buttonOnClick} className={`hamburger ${isActive ? "active" : ''}`}>
        <span className="one"></span>
        <span className="two"></span>
        <span className="three"></span>
      </div>
      <div className="title">
        <ul>
          <li>Research Professional</li>
          <li>Platform</li>
        </ul>
      </div>
      <nav className={`${isActive ? "active" : ''}`}>
        <Link to="" className="site-link">Site Link</Link>
        <ul>
          <li><Link to="">Home</Link></li>
          <li><Link to="">About Us</Link></li>
          <li><Link to="">Insights</Link></li>
          <li><Link to="">Events</Link></li>
          <li><Link to="">Contact Us</Link></li>
        </ul>
      </nav>
    </header>
  )
}